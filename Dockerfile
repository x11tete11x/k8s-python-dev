FROM x11tete11x/busybox
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    openssh-server \
    python3 \
    python3-pip \
    virtualenv \
    && rm -rf /var/lib/apt/lists/*
RUN echo 'root:root' | chpasswd
RUN echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config
RUN pip install supervisor
RUN mkdir -p /var/run/sshd /var/log/supervisor
COPY ./supervisord.conf /etc/supervisor/supervisord.conf
COPY ./scripts/* /opt/scripts/
RUN chmod +x /opt/scripts/*
WORKDIR /root
ENTRYPOINT ["/usr/local/bin/supervisord"]
