# k8s-python-dev

A dev pod loaded with python and virtualenv to test your aplicacionts directly inside a pod

## Getting started

Deploy with:
```
kubectl apply -k . -n <namespace>
```

Then you can access with ssh:
```
ssh -p 30022 root@IP
```
The `root` password it's `root`
